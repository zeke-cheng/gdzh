## Git 客户端使用文档 ##




### Git客户端的下载和安装 ###

- [Mac OS X](https://git-scm.com/download/mac)
- [Windows](https://git-scm.com/download/win)
- [Linux/Unix](https://git-scm.com/download/linux)

以Windows平台为例子，下载安装[64-bit Git for Windows Setup](https://github.com/git-for-windows/git/releases/download/v2.27.0.windows.1/Git-2.27.0-64-bit.exe)默认配置安装后提供Git GUI和Git Bash客户端


### 生成SSH RSA Key ###

注意异地备份本机的RSA Key，若无则创建

Windows客户端 RSA Key的目录位置，例如系统chris用户：C:\Users\chris\\.ssh




Git Bash 命令行客户端创建RSA Key

- ssh-keygen.exe -t rsa

Git GUI 图形客户端创建RSA Key

- Help-Show SSH key- Generay Key


### 测试远端Git仓库权限 ###

上传前更新pub Key最后的备注，例如:zhiheng.zheng@wakashuju.com

上传id_rsa.pub到LDAP服务器

测试： "git clone ssh://git@repo.wukashuju.com/source/newfhbadminTest.git"

备注：Git repos的连接需要基于公司光纤内网或者VPN

测试正常使用后，另将pub Key发至运维邮箱做保存记录zhiheng.zheng@wakashuju.com


