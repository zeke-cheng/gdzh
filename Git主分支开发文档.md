## Git 主分支开发文档 ##


### 旧SVN代码提交流程：

1. local branch A  push --> origin branch A

2. local branch master push --> origin master

### 新Git代码提交流程
（上面SVN第二步将被禁止，若要提交到master分支，按以下操作)

1. local branch A clone from origin master

2. local branch A push --> origin A

3. origin A merge --> origin master

### 代码提交参考

创建分支zzh-draft
>git branch zzh-draft

切换到分支zzh-draft并同步master的代码
>git checkout zzh-draft

>git fetch origin

新建代码文件的提交
>git checkout zzh-draft

>git add StoreApiVM.cs

>git commit StoreApiVM.cs -m "上线识别脸交易功能"

>git push origin zzh-draft

>git fetch origin

>git rebase origin/master

已存在代码文件的提交
>git checkout zzh-draft

>git commit StoreApiVM.cs -m "上线识别脸交易功能"

>git push origin zzh-draft

>git fetch origin

>git rebase origin/master





